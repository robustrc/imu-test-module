//... Standard includes
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

//...Eigen include
#include "/usr/include/eigen3/Eigen/Dense"
using namespace Eigen;

//...ROS includes
#include "/opt/ros/lunar/include/ros/ros.h"
#include "/opt/ros/lunar/include/tf/tf.h"
#include "/opt/ros/lunar/include/geometry_msgs/Vector3Stamped.h"
#include "/opt/ros/lunar/include/geometry_msgs/AccelStamped.h"

geometry_msgs::Vector3Stamped cam[2];
geometry_msgs::Vector3Stamped cam_old[2];
geometry_msgs::Vector3Stamped imu[2];


void camCallback0(const geometry_msgs::Vector3Stamped camData0) {
  if ((camData0.vector.x > 0) && (camData0.vector.y > 0))  {
    cam[0] = camData0;  }
  }

  void camCallback1(const geometry_msgs::Vector3Stamped camData1) {
    if ((camData1.vector.x > 0) && (camData1.vector.y > 0))  {
      cam[1] = camData1;  }
    }

    int main( int argc, char** argv ) {
      ros::init(argc, argv, "imu_test_module");
      ros::NodeHandle n;
      ros::Rate loop_rate(500); // rate in Hz

      bool runonce = true;
      float dt = 0;

      // Create publisher
      ros::Publisher IMUVectorPub0 = n.advertise<geometry_msgs::Vector3Stamped>("imuData0", 1);
      ros::Publisher IMUVectorPub1 = n.advertise<geometry_msgs::Vector3Stamped>("imuData1", 1);

      // subscribe to topic published by camera.
      ros::Subscriber subCam0 = n.subscribe("car0PoseFromCam", 1, camCallback0);
      ros::Subscriber subCam1 = n.subscribe("car1PoseFromCam", 1, camCallback1);

      while (ros::ok()) {
        if (runonce) {
          // wait for a message from topic car0PoseFromCam to set initial values
          boost::shared_ptr<geometry_msgs::Vector3Stamped const> msgRecived;
          msgRecived = ros::topic::waitForMessage<geometry_msgs::Vector3Stamped>("car0PoseFromCam");
          runonce = false;
          cam_old[0] = cam[0];
          cam_old[1] = cam[1];
          std::cout << "I did a thing once" << '\n';
        }
        for (size_t i = 0; i < 2; i++) {
          if (cam[i].header.seq > cam_old[i].header.seq) {
            dt = cam[i].header.stamp.toSec() - cam_old[i].header.stamp.toSec();
            // Publish calculated data
            imu[i].header.stamp = cam[i].header.stamp;
            imu[i].vector.x = ((cam[i].vector.x-cam_old[i].vector.x)/(dt*dt))*cos(cam[i].vector.z);
            imu[i].vector.y = ((cam[i].vector.y-cam_old[i].vector.y)/(dt*dt))*sin(cam[i].vector.z);
            imu[i].vector.z = (cam[i].vector.z-cam_old[i].vector.z)/dt;
            cam_old[i] = cam[i];

            if (i == 0)
            {
              imu[i].header.frame_id = "0";
              IMUVectorPub0.publish(imu[i]);
            }
            else if (i == 1)
            {
              imu[i].header.frame_id = "1";
              IMUVectorPub1.publish(imu[i]);
            }
          }
        }

        ros::spinOnce();
        loop_rate.sleep();
      }
    }
